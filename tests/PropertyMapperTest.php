<?php

namespace Maximaster\Tests;

use Exception;
use Maximaster\Mapper\MapperConfigurator;
use PHPUnit\Framework\TestCase;
use AutoMapperPlus\AutoMapper;

class PropertyMapperTest extends TestCase
{
    public function testMap()
    {
        $mapperConfigurator = new MapperConfigurator(User::class, ListUserView::class);
        $config = $mapperConfigurator->getConfig();

        $mapper = new AutoMapper($config);
        $sourceObj = new User(1995);

        try {
            $destinationObj = $mapper->map($sourceObj, ListUserView::class);

            $this->assertIsObject($destinationObj);
            $this->assertObjectHasAttribute('yearsOld', $destinationObj);
            $this->assertSame(25, $destinationObj->yearsOld);
        } catch (Exception $exception) {
            $this->fail($exception->getMessage());
        }
    }
}