<?php

namespace Maximaster\Tests;


class User
{
    /** @var int */
    private $birthOn;

    public function __construct(int $birthOn)
    {
        $this->birthOn = $birthOn;
    }

    /**
     * @return int
     */
    public function getYearsOld(): int
    {
        return (int)date('Y') - $this->birthOn;
    }
}