<?php

namespace Maximaster\Mapper;

use AutoMapperPlus\Configuration\AutoMapperConfig;
use AutoMapperPlus\Configuration\MappingInterface;
use Exception;
use ReflectionClass;
use ReflectionException;

class MapperConfigurator
{
    /** @var string */
    private $sourceClassName;

    /** @var string */
    private $destinationClassName;

    /** @var string */
    private const GETTER_NAME_START = '/^get/mi';

    /** @var AutoMapperConfig */
    private $config;

    /**
     * MapperConfig constructor.
     * @param string $sourceClassName
     * @param string $destinationClassName
     */
    public function __construct(string $sourceClassName, string $destinationClassName)
    {
        $this->sourceClassName = $sourceClassName;
        $this->destinationClassName = $destinationClassName;

        $this->execute();
    }

    /**
     * Регистрирует mapper для переданных классов
     */
    private function registerMapping()
    {
        $this->config->registerMapping($this->sourceClassName, $this->destinationClassName);
    }

    /**
     * Получает названия getter'ов из ресурсного класса
     *
     * @return string[] $methods
     * @throws ReflectionException
     */
    private function getGettersNames(): array
    {
        $sourceClass = new ReflectionClass($this->sourceClassName);
        $methods = $sourceClass->getMethods();

        $methods = array_map(function ($method) {
            return $method->name;
        }, $methods);

        $methods = array_filter($methods, function ($method) {
            return $method !== '__construct' && preg_match(self::GETTER_NAME_START, $method);
        });

        return $methods;
    }

    /**
     * Получает свойства класса назначения совпадающие с getter'ами рессурсного класса
     *
     * @param string[] $gettersNames
     * @return string[] $properties
     * @throws ReflectionException
     */
    private function getPropertiesMatchingGetters(array $gettersNames)
    {
        $destinationClass = new ReflectionClass($this->destinationClassName);
        $properties = $destinationClass->getProperties();

        $properties = array_map(function ($property) {
            return $property->name;
        }, $properties);

        $properties = array_filter($properties, function ($property) use ($gettersNames) {
            return !is_null($this->getGetterByProperty($property, $gettersNames));
        });

        return $properties;
    }

    /**
     * Получает getter ресурсного класса в соответствии со свойством класса назначения
     *
     * @param string $property
     * @param string[] $getters
     * @return string|null
     */
    private function getGetterByProperty(string $property, array $getters)
    {
        $getters = array_filter($getters, function ($getter) use ($property) {
            $getterBody = preg_replace(self::GETTER_NAME_START, '', $getter);
            return strtolower($getterBody) === strtolower($property);
        });

        if (sizeof($getters) === 0) {
            return null;
        }

        return current($getters);
    }

    /**
     * Получает mapper зарегистрированных классов
     *
     * @return MappingInterface
     * @throws Exception
     */
    private function getMapper()
    {
        $mapper = $this->config->getMappingFor($this->sourceClassName, $this->destinationClassName);

        if (is_null($mapper)) {
            throw new Exception('Mapper\'s not been found');
        }

        return $mapper;
    }

    /**
     * Добавляет в текущий mapper конфигурацию для getter'ов ресурсоного класса и свойств класса назначения
     *
     * @param MappingInterface $mapper
     * @param string[] $getters
     * @param string[] $properties
     * @throws Exception
     */
    private function extendMapper(MappingInterface $mapper, array $getters, array $properties)
    {
        if (sizeof($getters) === 0) {
            throw new Exception('Source\'s getters\'ve not found');
        }

        if (sizeof($properties) === 0) {
            throw new Exception('Destination\'s properties\'ve not found');
        }

        array_walk($properties, function ($property) use ($getters, $mapper) {
            $getter = $this->getGetterByProperty($property, $getters);

            $mapper->forMember($property, function ($source) use ($getter) {
                return $source->$getter();
            });
        });
    }

    /**
     * Главный метод для получения и модификации конфигурации
     */
    private function execute()
    {
        try {
            $this->config = new AutoMapperConfig();
            $this->registerMapping();

            $gettersNames = $this->getGettersNames();
            $properties = $this->getPropertiesMatchingGetters($gettersNames);
            $mapper = $this->getMapper();

            $this->extendMapper($mapper, $gettersNames, $properties);
        } catch (Exception $exception) {
            print_r($exception->getMessage());
        }
    }

    /**
     * @return AutoMapperConfig
     */
    public function getConfig()
    {
        return $this->config;
    }
}